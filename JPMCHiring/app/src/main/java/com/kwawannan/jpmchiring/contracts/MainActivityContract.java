package com.kwawannan.jpmchiring.contracts;

import com.kwawannan.jpmchiring.model.HighSchool;

import java.util.List;

/**
 * Contract for MVP layer
 */

public interface MainActivityContract {

    interface View {
        void  initMainActivity();
        void updateSchoolList(List<HighSchool> schools);
    }

    interface Presenter {
        void addView(View view);

        void getNycSchools();


    }
}