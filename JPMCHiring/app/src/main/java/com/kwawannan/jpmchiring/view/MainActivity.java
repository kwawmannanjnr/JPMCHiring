package com.kwawannan.jpmchiring.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.kwawannan.jpmchiring.adapter.ResultsAdapter;
import com.kwawannan.jpmchiring.contracts.MainActivityContract;
import com.kwawannan.jpmchiring.model.HighSchool;
import com.kwawannan.jpmchiring.model.SatScore;
import com.kwawannan.jpmchiring.R;
import com.kwawannan.jpmchiring.presenter.MainActivityPresenter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View {

    String LOG_TAG;

    MainActivityPresenter presenter;

    RecyclerView recyclerView;

    ProgressBar progressBar;

    ResultsAdapter resultsAdapter;
    //    private List<HighSchool> highSchoolList;
    private List<SatScore> satScoreList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Store name of the activity
        LOG_TAG = this.getLocalClassName();

        presenter = new MainActivityPresenter();
        presenter.addView(this);
        presenter.initView();
        presenter.initService();
        presenter.getNycSchools();

    }

    @Override
    public void initMainActivity() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        final GridLayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        progressBar = (ProgressBar) findViewById(R.id.showSpinner);
    }

    @Override
    public void updateSchoolList(List<HighSchool> schools) {
        resultsAdapter = new ResultsAdapter(schools);
        recyclerView.setAdapter(resultsAdapter);
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }
}
