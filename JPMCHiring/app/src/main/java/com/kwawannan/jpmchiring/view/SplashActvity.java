package com.kwawannan.jpmchiring.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;

import org.jetbrains.annotations.Nullable;

public class SplashActvity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, MainActivity.class);
        SystemClock.sleep(3000);
        startActivity(intent);
        finish();

    }
}
