package com.kwawannan.jpmchiring.api_client;

import com.kwawannan.jpmchiring.model.HighSchool;
import com.kwawannan.jpmchiring.model.SatScore;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("s3k6-pzi2.json")
    Call<List<HighSchool>> getHighSchools();

    @GET("f9bf-2cp4.json")
    Call<List<SatScore>> getSatScores();


}
